# frozen_string_literal: true

require_relative "invoice_generator/version"
require_relative "invoice_generator/template_generator"
module InvoiceGenerator
  class Error < StandardError
  end
end
