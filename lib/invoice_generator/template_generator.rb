# frozen_string_literal: true

require "csv"
require "prawn"
require "prawn/table"
require_relative "formatted_json_data"

module InvoiceGenerator
  class TemplateGenerator
    include FileReader
    include FormattedJsonData

    attr_accessor :file_path

    def initialize(file_path)
      @file_path = file_path
    end

    def color
      { color_config: { header1_row_color: "ed6161", header1_cell_text_color: "FFFFFF", header1_border_color: "ed6161",
                        header2_text_color: "FFFFFF", header2_row_color: "e54e4e", header2_border: "e54e4e",
                        table_title_text: "9d999e", table_border: "9d999e", sub_total_table_border: "e0dcdc",
                        bill_to: "9d999e", footer_border: "e0dcdc", worker_row: "e0dcdc", worker_even_row: "f0f1f2" } }
    end

    def pdf_balance_due(projects_key, pdf)
      payment_due = [["Invoice Number:", (projects_key[:invoice]).to_s], ["P.O./S.O. Number:", "KJS29094"],
                     ["Invoice Date:", "2015-08-22"], ["Payment Due:", "Upon receipt"]]
      pdf.float do
        pdf.table(payment_due, position: :right, cell_style: { border_width: 0 }) do |table|
          table.column(0).style align: :right, font_style: :bold
          table.column(1).style align: :left, padding_right: 7
        end
      end
    end

    def pdf_header1_table(projects_key)
      [["Invoice"], [(projects_key[:client][:name]).to_s]]
    end

    def pdf_header1(projects_key, pdf, colour)
      header1 = pdf_header1_table(projects_key)
      pdf.table(header1,
                row_colors: [(colour[:color_config][:header1_row_color]).to_s],
                cell_style: { width: 350, text_color: (colour[:color_config][:header1_cell_text_color]).to_s,
                              padding: [0, 0, 0, 30], position: :left,
                              border_color: (colour[:color_config][:header1_border_color]).to_s }) do |header1_table|
        header1_table.row(0).style size: 27, height: 40, padding: [10, 0, 0, 30]
        header1_table.row(1).style height: 40
      end
    end

    def pdf_header2(projects_key, pdf, colour)
      header2 = [["Amount Due"], ["$#{projects_key[:total]}"]]
      pdf.float do
        pdf.table(header2,
                  position: :right,
                  row_colors: [(colour[:color_config][:header2_row_color]).to_s],
                  cell_style: { width: 200, text_color: (colour[:color_config][:header2_text_color]).to_s,
                                padding: [0, 0, 0, 30],
                                border_color: (colour[:color_config][:header2_border]).to_s }) do |header2_table|
          header2_table.row(0).style height: 40, padding: [15, 0, 0, 50]
          header2_table.row(1).style height: 40, size: 23, padding: [0, 0, 15, 30]
        end
      end
    end

    def pdf_client(pdf, projects_key)
      pdf.text "BILL TO:", size: 11, color: (color[:color_config][:bill_to]).to_s
      pdf.text "<b>#{projects_key[:client][:name]}</b>", size: 13, inline_format: true
      pdf.text "Reduct.nepal@gmail.com", size: 11
      pdf.text "514-423-1235", size: 11
      pdf.move_down 10
      pdf.text (projects_key[:client][:address]).to_s, size: 11
      pdf.text "KTM, Nepal", size: 11
    end

    def title(pdf, colour)
      title = [["PHOTO SERVICES", "QUANTITY", "RATE", "AMOUNT"]]
      pdf.table(title, width: 532,
                       cell_style: { text_color: (colour[:color_config][:table_title_text]).to_s, size: 13,
                                     border_width: 0, border_bottom_width: 3,
                                     border_color: (colour[:color_config][:table_border]).to_s }) do
        style(column(0), width: 220)
        style(column(1..3), width: 104, align: :right)
      end
    end

    def pdf_user_table_data(pdf, row_color, data, colour)
      pdf.table(data, row_colors: [row_color], width: 532,
                      cell_style: { size: 13, border_width: 0, border_bottom_width: 1,
                                    border_color: (colour[:color_config][:table_title_text]).to_s }) do
        style(column(0), font_style: :bold, width: 220)
        style(column(1..3), width: 104, align: :right)
      end
    end

    def pdf_user_table(pdf, worker, index)
      colour = color
      data = [[{ content: (worker[:name]).to_s.titlecase, size: 16 }, (worker[:quantity]).to_s, "$#{worker[:rate]}",
               "$#{worker[:total]}"]]

      row_color = if index.even?
                    (colour[:color_config][:worker_even_row]).to_s
                  else
                    (colour[:color_config][:header1_cell_text_color]).to_s
                  end
      pdf_user_table_data(pdf, row_color, data, colour)
    end

    def pdf_subtotal_border_style(table, colour)
      table.row(2).style border_top_width: 1, border_bottom_width: 3,
                         border_color: (colour[:color_config][:sub_total_table_border]).to_s
    end

    def pdf_sub_total_section(projects_key, pdf, colour)
      sub_total = sub_total_table(projects_key)
      pdf.table(sub_total, position: :right, cell_style:
          { border_width: 0, border_color: (colour[:color_config][:table_border]).to_s }) do |table|
        table.column(0).style font_style: :bold, align: :right
        table.column(1).style align: :right, padding_left: 40
        pdf_subtotal_border_style(table, colour)
        table.column(1).row(2).style padding_right: -40
        table.column(1).row(3).style font_style: :bold, padding_left: 80
      end
    end

    def pdf_footer_table(projects_key)
      logo = Rails.root + ENV["LOGO_FILE_PATH"]
      [["", (projects_key[:provider][:name]).to_s, "Contact Information"],
       [{ image: logo, fit: [80, 250] }, "Ranibari,Samakhusi", "980-3572935"],
       ["", "KTM,Nepal", "www.truemark.dev"]]
    end

    def pdf_footer(projects_key, pdf, colour)
      footer = pdf_footer_table(projects_key)
      pdf.table(footer, width: 535, cell_style: { border_width: 0 }) do
        style(row(0), border_top_width: 1, border_color: (colour[:color_config][:footer_border]).to_s,
                      font_style: :bold)
        style(column(1..2), padding: [15, 0, 0, 70])
        style(row(1).column(1..2), padding: [5, 0, 0, 70])
        style(row(2), padding_top: 0)
        style(column(2), align: :right)
      end
    end

    def pdf_part_header(value, pdf, colour)
      pdf_header2(value, pdf, colour)
      pdf_header1(value, pdf, colour)
      pdf.move_down 20
      pdf_balance_due(value, pdf)
      pdf_client(pdf, value)
    end

    def pdf_middle_part(value, pdf, colour)
      pdf.move_down 30
      title(pdf, colour)
      value[:workers].each_with_index do |worker, index|
        pdf_user_table(pdf, worker, index)
      end
      pdf.move_down 30
      pdf_sub_total_section(value, pdf, colour)
    end

    def pdf_footer_part(value, pdf, colour)
      pdf_footer(value, pdf, colour)
    end

    def pdf_styling(pdf_file_path, value)
      Prawn::Document.generate(pdf_file_path) do |pdf|
        colour = color
        pdf_part_header(value, pdf, colour)
        pdf_middle_part(value, pdf, colour)
        pdf.move_down 30
        pdf.move_down 5
        pdf_footer_part(value, pdf, colour)
      end
    end

    def create_organization_folder(organization_name)
      organization_name_folder = organization_name.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
      organization_folder = "#{Rails.root}/data/#{organization_name_folder}"
      Dir.mkdir(organization_folder) unless File.exist?(organization_folder)
    end

    def create_invoices_folder(organization_name_folder, project_name_format)
      invoices_folder_path = "#{Rails.root}/data/#{organization_name_folder}/#{project_name_format}/invoices"
      Dir.mkdir(invoices_folder_path) unless File.exist?(invoices_folder_path)
    end

    def pdf_generate(organization_name)
      projects = format_projects_data
      projects.each do |key, value|
        organization_name_folder = organization_name.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
        create_organization_folder(organization_name)
        project_name_format = key.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
        project_folder = "#{Rails.root}/data/#{organization_name_folder}/#{project_name_format}"
        Dir.mkdir(project_folder) unless File.exist?(project_folder)
        create_invoices_folder(organization_name_folder, project_name_format)
        pdf_path = "#{Rails.root}/data/#{organization_name_folder}/#{project_name_format}/invoices/#{start_date}.pdf"
        pdf_styling(pdf_path, value)
      end
    end
  end
end
