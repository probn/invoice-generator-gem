# frozen_string_literal: true

require "csv"
require "prawn"
require "prawn/table"
require_relative "formatted_json_data"

module InvoiceGenerator
  class PdfGenerator
    include FileReader
    include FormattedJsonData

    attr_accessor :file_path

    def initialize(file_path)
      @file_path = file_path
    end

    def pdf_logo(pdf)
      logo = Rails.root + ENV["LOGO_FILE_PATH"]
      pdf.image logo, width: 100
    end

    def color
      { color_config: { table_header: "0f3041", table_text: "FFFFFF", sub_total_table_border: "D3D3D3",
                        table_row_workers: "F0F0F0" } }
    end

    def pdf_header(projects_key, pdf)
      header = [["INVOICE:", (projects_key[:invoice]).to_s], ["Date:", "Apr 19, 2021"], ["Due Date:", "Apr 26, 2021"],
                ["Balance Due:", "$#{projects_key[:total]}"]]
      pdf.float do
        pdf.table(header, position: :right, cell_style: { border_width: 0 },
                          row_colors: [nil, nil, nil, (color[:color_config][:sub_total_table_border]).to_s]) do
          style(column(0), align: :left, font_style: :bold)
          style(column(1), align: :right)
        end
      end
    end

    def pdf_provider_and_client(pdf, provider, client)
      pdf.text "<b>#{provider[:name]}</b>", size: 13, inline_format: true
      pdf.text (provider[:address]).to_s, size: 11
      pdf.text "VAT:#{provider[:vat]}", size: 11
      pdf.move_down 10
      pdf.text "<b>Bill To:</b>", size: 11, inline_format: true
      pdf.move_down 5
      pdf.text "<b>#{client[:name]}</b>", size: 13, inline_format: true
      pdf.text (client[:address]).to_s, size: 11
      pdf.text "VAT:#{client[:vat]}", size: 1
    end

    def pdf_user_table_data(worker, pdf, colour)
      worker.each do |workers|
        data = [[(workers[:name]).to_s, (workers[:quantity]).to_s, "$#{workers[:rate]}",
                 "$#{workers[:total]}"]]
        pdf.table(data, row_colors: [(colour[:color_config][:table_row_workers]).to_s],
                        column_widths: [127, 138, 133, 141], cell_style: { size: 13, border_width: 0 }) do
          style(row(0).column(0), font_style: :bold)
        end
      end
    end

    def pdf_user_table(pdf, worker)
      colour = color
      title = [%w[Item Quantity Rate Amount]]
      pdf.table(title, row_colors: [(colour[:color_config][:table_header]).to_s],
                       column_widths: [127, 138, 133, 141],
                       cell_style: { text_color: (colour[:color_config][:table_text]).to_s, size: 13, border_width: 0 })
      pdf_user_table_data(worker, pdf, colour)
    end

    def pdf_sub_total_section(key, pdf)
      colour = color
      sub_total_table = sub_total_table(key)
      pdf.table(sub_total_table, position: :right, cell_style: { border_width: 0 }) do
        style(column(0), font_style: :bold, align: :left)
        style(column(1), align: :right)
        style(row(2), border_top_width: 1, border_bottom_width: 2,
                      border_color: (colour[:color_config][:sub_total_table_border]).to_s)
      end
    end

    def pdf_footer(pdf)
      pdf.text "<b>Notes:</b>", size: 11, inline_format: true
      pdf.text "Pay to our bank:", size: 11
      pdf.text "Global Ime Bank", size: 11
      pdf.text "Branch: Newroad:", size: 11
      pdf.text "Acc Number: 2230232323", size: 11
    end

    def pdf_styling(pdf_file_path, value)
      Prawn::Document.generate(pdf_file_path) do |pdf|
        pdf_logo(pdf)
        pdf.move_down 20
        pdf_header(value, pdf)
        pdf_provider_and_client(pdf, value[:provider], value[:client])
        pdf.move_down 40
        pdf_user_table(pdf, value[:workers])
        pdf.move_down 10
        pdf_sub_total_section(value, pdf)
        pdf_footer(pdf)
      end
    end

    def create_organization_folder(organization_name)
      organization_name_folder = organization_name.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
      Dir.mkdir("#{Rails.root}/#{organization_name_folder}") unless File.exist?(organization_name_folder)
    end

    def generate_pdf(organization_name)
      projects = format_projects_data
      projects.each do |key, value|
        create_organization_folder(organization_name)
        organization_name_folder = organization_name.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
        project_name_format = key.downcase.gsub!(/[^a-z0-9\-_]+/, "-")
        project_folder = "#{Rails.root}/#{organization_name_folder}/#{project_name_format}"
        Dir.mkdir(project_folder) unless File.exist?(project_folder)
        root_folder_path = "#{Rails.root}/#{organization_name_folder}"
        pdf_file_path = "#{root_folder_path}/#{project_name_format}/#{start_date}.pdf"
        pdf_styling(pdf_file_path, value)
      end
    end
  end
end
