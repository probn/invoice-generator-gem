# frozen_string_literal: true

module FormattedJsonData
  def line_item_extracter(project, worker_name, row)
    project[:workers].select { |worker| worker[:name] == worker_name }.first || {
      project: row["project"],
      name: row["workers"],
      duration_seconds: 0,
      quantity: 0,
      currency: "$",
      rate: 100,
      total: 0,
      quantity_display: "Hours",
      rate_display: "rate per hour"
    }
  end

  def projects_data(row_project, line_items, projects)
    projects[row_project.to_s] = {
      name: row_project,
      workers: line_items.values.select { |line_item| line_item[:project] == row_project },
      sub_total: 0,
      tax_rate: 0.13,
      tax_amount: 0,
      discount: 0,
      total: 0,
      invoice: 1,
      provider: { name: "Truemark Private Limited", address: "Ranibari,KTM", vat: "665789012" },
      client: { name: "Reduct Nepal", address: "Mandikhatar,KTM", vat: "69708123" }
    }
  end

  def duration_hour(line_item, row)
    duration_seconds = line_item[:duration_seconds] + row["duration_seconds"].to_f
    line_item[:duration_seconds] = duration_seconds
    duration_hours = (duration_seconds / (60 * 60)).round(1)
    line_item[:quantity] = duration_hours
  end

  def calculate_duration_hour(line_item, row, projects)
    if (projects[(row["project"]).to_s] && projects[(row["project"]).to_s][:name]) == row["project"] &&
       line_item[:name] == row["workers"]
      duration_hour(line_item, row)
    end
  end

  def format_json
    projects = {}
    line_items = {}
    time_log_data = log_file
    time_log_data.each do |row|
      project = projects[(row["project"]).to_s] || { workers: [] }
      line_item = line_item_extracter(project, row["workers"], row)
      line_items["#{row["project"]} #{row["workers"]}"] = line_item
      projects_data(row["project"], line_items, projects)
      calculate_duration_hour(line_item, row, projects)
    end
    projects
  end

  def calculate_total_hour(project, rates_each)
    project[:workers].each do |worker|
      next unless worker[:name] == rates_each["resource_name"] && project[:name] == rates_each["project_name"]

      worker[:rate] = rates_each["rate_per_hour"]
      total = worker[:rate].to_f * worker[:quantity].to_f
      worker[:total] = total
    end
  end

  def rates_file_array(projects)
    rates_file_array = rates_file
    rates_file_array.each do |rates_each|
      projects.each do |_key, project|
        calculate_total_hour(project, rates_each)
      end
    end
  end

  def total_project_cost(projects, sub_total)
    projects[:sub_total] = sub_total
    tax_rate = projects[:tax_rate]
    tax_amount = (tax_rate * sub_total).round(2)
    projects[:tax_amount] = tax_amount
    total = sub_total - tax_amount
    projects[:total] = total
  end

  def format_projects_data
    # return json of two projects
    projects = format_json
    rates_file_array(projects)
    projects.each do |key, _value|
      sub_total = 0
      projects[key][:workers].each do |workers|
        sub_total += workers[:total]
      end
      total_project_cost(projects[key], sub_total)
    end

    projects
  end
end

module FileReader
  include FormattedJsonData
  MONTH_TEXT = { 1 => "january", 2 => "febuary", 3 => "march", 4 => "april", 5 => "may", 6 => "june", 7 => "july",
                 8 => "august", 9 => "september", 10 => "october", 11 => "november", 12 => "december" }.freeze

  def log_file
    CSV.foreach(Rails.root + file_path, headers: true)
  end

  def rates_file
    CSV.foreach(Rails.root + ENV["RATES_FOLDER_PATH"], headers: true).map(&:to_h)
  end

  def start_date
    start_time = []
    end_time = []
    log_file.each do |row|
      start_time << row["start_time"]
      end_time << row["end_time"]
    end
    starting_date = start_time.min
    ending_date = end_time.max
    date_format(starting_date, ending_date)
  end

  def date_format(starting_date, ending_date)
    start_year = starting_date[0..4]
    start_month = starting_date[5..6].to_i
    start_month_text = MONTH_TEXT[start_month]
    end_year = ending_date[0..4]
    end_month = ending_date[5..6].to_i
    end_month_text = MONTH_TEXT[end_month]
    start_year << start_month_text << "_" << end_year << end_month_text
  end

  def sub_total_table(key)
    [["Subtotal:", "$#{key[:sub_total]}"], ["Tax (13%):", "$#{key[:tax_amount]}"],
     ["Total:", "$#{key[:total]}"], ["Amount Due:", "$#{key[:total]}"]]
  end
end
